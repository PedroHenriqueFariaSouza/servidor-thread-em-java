/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Threads;

import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author ADM
 */
public class cliente2 {
    public static void main(String[] args) 
    {
        try 
        {
            Socket cliente = new Socket("127.0.0.1", 12345); //primeiro parametro e o IP que quero conectar nesse caso puis IP do localhost e na mesma porta pra efetivar a conexão.
            System.out.println("Cliente 2 tentando conecxao.\n");
            
            ObjectInputStream entrada = new ObjectInputStream(cliente.getInputStream()); //mesma explicação que no servidorSimples enquanto la era um OUTPUT (uma saida) aqui é uma entrada (input).
            
            Date data_atual = (Date)entrada.readObject(); //aqui peguei a entrada que veio do servidor instanciando ENTRADA e pedi pra ler o objeto com o readObject().
            JOptionPane.showMessageDialog(null,"Data recebida do servidor: " + data_atual.toString());
            //JoptionPane é apenas para jogar numa telinha a mensagem recebida do servidor.
            
            entrada.close();
            System.out.println("Conexão encerrada.\n");
        }
        catch(Exception e) 
        {
            System.out.println("Erro: " + e.getMessage());
            e.printStackTrace();
        }
    }
    
}
