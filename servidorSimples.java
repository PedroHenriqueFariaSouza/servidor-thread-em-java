/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Threads;

import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

/**
 *
 * @author ADM
 */
public class servidorSimples {
    public static void main(String[] args) 
    {
        try 
        {
            //Instancia o serverSocket ouvindo a porta 12345
            ServerSocket servidor = new ServerSocket(12345); //n preciso passar o ip porque ele considera o IP local
            System.out.println("Servidor ouvindo a porta 12345");
            while(true) //repetição pra ele ficar ouvindo requisições
            {
                //o método accept() bloqueia a execução ate que o servidor receba um pedido de conexão
                Socket conexao = servidor.accept();
                System.out.println("Cliente conectado no IP: " + conexao.getInetAddress().getHostAddress());
                
                //Emulando o GARGALO (descomentar as linhas seguintes de codigo)
                //suponhamos que esse servidor processe algo hiper demorado como uma formula complexa demais
                //e demande muito tempo.
                //int i=0;
                //while(i<1) {}//GARGALO da conexão.
                //nesse caso quando eu aceionar cliente1 ele vai executar cliente mas n irá devolver
                //a resposta(data) porque eu fiz o gargalo proposital para emular um processamento longo demais.
                //Se eu acionar cliente 2 o servidorSimples n irá identificar o cliente2 acessando porque
                //cliente1 tomou conta do MONOPROCESSAMENTO do servidor (sem threads).
                //ai que o servidor thread nos ajuda que independentes de qnts conexões ocorra nunca vai travar em uma.
                
                ObjectOutputStream saida = new ObjectOutputStream(conexao.getOutputStream());
                //O objectOutputStream é um objeto java como se fosse um cano para transmissão dos dados
                //dentro do socket. O objeto SAIDA na linha 31 quando instanciou ele com o objectoutputstream
                //dentro dos parenteses ele recebe como parametro no proprio construtor o socket que instanciei
                //como nome CONEXAO.
                saida.flush();
                saida.writeObject(new Date()); //pegando hora e data pra enviar pro cliente
                saida.close();
                
                conexao.close();
            }
        }
        catch(Exception e) 
        {
            System.out.println("Erro: " + e.getMessage());
        }
    }
    
}
