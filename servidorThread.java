/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Threads;

import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

/**
 *
 * @author ADM
 */
public class servidorThread extends Thread {
    
    private Socket conexao;
    
    public servidorThread(Socket c) {
        this.conexao = c;
    }
    
    //La em cima no extends Thread para realmente funcionar corretamente precisamos implementar uma
    //função pra dar tudo certo e ela é essa abaixo de nome run().
    @Override
    public void run() {
        try 
        {
            System.out.println("Thread criada.\n");
            
            //agora vamos emular o mesmo GARGALO que o servidorSimples para analisar e comparar o comportamento.
            //descomentar abaixo o codigo pro gargalo funcionar.
            //int i=0;
            //while (i<1) {
                //GARGALO
            //}
            
            ObjectOutput saida = new ObjectOutputStream(conexao.getOutputStream());
            saida.flush();
            saida.writeObject(new Date());
            saida.close();
            //Essas 4 linhas acima é o que vai rodar em paralelo caso um dos processamentos demore mais
            //o que na verdade é o GARGALO que emulei no servidorSimples.
            //No caso do servidorSimples ele aceita conexão do cliente1 e o mesmo domina o processamento
            //em outras palavras 1 thread dominiou por completo e ele ficará assim até terminar seu 
            //processamento ja aqui o método run() vai fazer que caso uma thread fique ocupada ela
            //dispara outras threads para executar o codigo que APENAS AQUI CONTEM.
        } 
        catch (IOException e) {
            System.out.println("Houve um erro: " + e.getMessage() + "\n");
        }
    }
    
    public static void main(String args[]) 
    {       //lembrando a thread é o codigo que vc quer executar ficar dentro do metodo run().
            //o restante do codigo como estabelecer a conexão é na main (servidor).
        try 
        {
            ServerSocket servidor = new ServerSocket(12345);
            System.out.println("Servidor ouvindo a porta 12345.\n");
            
            while(true) 
            {
                Socket conexao = servidor.accept();
                System.out.println("Cliente conectado: " + conexao.getInetAddress().getHostAddress() + "\n");
                
                Thread t = new servidorThread(conexao); //eu criei um objeto da minha classe aqui que estamos.
                t.start(); //é aqui que ele chama o metodo run() dali acima.
            }
        }
        catch (IOException e) 
        {
            e.printStackTrace();
        }
    }
    
}
